'use strict';

/**
 * @ngdoc function
 * @name testProjectApp.controller:LogoutCtrl
 * @description
 * # LogoutCtrl
 * Controller of the testProjectApp
 */

angular.module('testProjectApp')
  .controller('LogoutCtrl', function ($scope, store, $location, UserService, auth) {
   // $scope.auth = auth;
    
  auth.signout();
  store.remove('profile');
  store.remove('token');
  store.remove('delegation_token');
  $location.path('/login');
  UserService.clearUser();
  });