'use strict';

/**
 * @ngdoc overview
 * @name testProjectApp
 * @description
 * # testProjectApp
 *
 * Main module of the application.
 */
angular
  .module('testProjectApp', [
    'auth0', 
    'angular-storage', 
    'angular-jwt',
    'ngRoute',
    'ngTouch'
  ])
  .config(function ($routeProvider, authProvider, $httpProvider, $locationProvider, jwtInterceptorProvider) {
    
    $routeProvider
      .when('/home', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        requiresLogin: true
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        requiresLogin: true
      })
      .when('/users', {
        templateUrl: 'views/users.html',
        controller: 'UsersCtrl',
        requiresLogin: true
      })
      .when('/user:userId', {
        templateUrl: 'views/user.html',
        controller: 'UserCtrl',
        requiresLogin: true
      })
      .when('/edit-user:userId', {
        templateUrl: 'views/editUser.html',
        controller: 'EditUserCtrl',
        requiresLogin: true
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .when('/logout', {
        templateUrl: 'views/about.html',
        controller: 'LogoutCtrl'
      })
      .when('/firebase', {
        templateUrl: 'views/firebase.html',
        controller: 'FirebaseCtrl',
        requiresLogin: true
      })
      .otherwise({
        redirectTo: '/home'
      });
      
    authProvider.init({
      domain: 'vradchenko.eu.auth0.com',
      clientID: 'ZdtnBi0gBlMc1BIJWUZRxr34w5aL6S09',
      loginUrl: '/login'
    });
    
    //Called when login is successful
    authProvider.on('loginSuccess', function($location, profilePromise, idToken, store, UserService) {
      profilePromise.then(function(profile) {
        store.set('profile', profile);
        store.set('token', idToken);
        UserService.setUser(profile);
      });
      $location.path('/home');
    });

    //Called when login fails
    authProvider.on('loginFailure', function($location) {
      $location.path('/login');
    });
    
    //Angular HTTP Interceptor function
    jwtInterceptorProvider.tokenGetter = function(store) {
        return store.get('token');
    };
    //Push interceptor function to $httpProvider's interceptors
    $httpProvider.interceptors.push('jwtInterceptor');
  })
  .run(function(auth, $rootScope, store, jwtHelper, $location) {
    // This hooks all auth events to check everything as soon as the app starts
    auth.hookEvents();
    
    $rootScope.$on('$locationChangeStart', function() {

      var token = store.get('token');
      if (token) {
        if (!jwtHelper.isTokenExpired(token)) {
          if (!auth.isAuthenticated) {
            //Re-authenticate user if token is valid
            auth.authenticate(store.get('profile'), token);
          }
        } else {
          // Either show the login page or use the refresh token to get a new idToken
          $location.path('/login');
        }
      }
    });
  });
