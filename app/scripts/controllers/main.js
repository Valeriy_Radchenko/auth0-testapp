'use strict';

/**
 * @ngdoc function
 * @name testProjectApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the testProjectApp
 */
angular.module('testProjectApp')
  .controller('MainCtrl', function ($scope, UserService, auth) {
      $scope.auth = auth;
      UserService.getUser().then(function(user) {
        $scope.user = user;
      });
  });
