'use strict';

/**
 * @ngdoc function
 * @name testProjectApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the testProjectApp
 */

angular.module('testProjectApp')
  .controller('LoginCtrl', function ($scope, auth, NetworkService, Config, store, $location, UserService) {
    $scope.auth = auth;
   
    $scope.form = {};
   
    $scope.signin = function(formData) {
      
      var data = {
        'client_id': Config.clientId,
        'username': formData.email,
        'password': formData.password,
        'connection': Config.connection,
        'grant_type': Config.grantType,
        'scope': Config.scope
      };
      
      NetworkService.post('/oauth/ro', data, function(result, response) {
        formData.errorMessage = false;
        if (result) {
          store.set('token', response.id_token);
          NetworkService.post('/tokeninfo', {id_token: response.id_token}, function(result, response) {
            if (result) {
              store.set('profile', response);
              UserService.setUser(response);
              $location.path('/home');
            }
          });
        } else {
          formData.errorMessage = response.error_description;
        }
      });
      
    };
    
    $scope.signup = function(formData) {
      formData.errorMessage = false;
      var data = {
        'client_id': Config.clientId,
        'email': formData.email,
        'password': formData.password,
        'connection': Config.connection
      };

      NetworkService.post('/dbconnections/signup', data, function(result, response) {
        if (result) {
          $scope.signin(formData);
        } else {
          formData.errorMessage = response.description || response.message || response.error;
        }
      });
    };
   
  });