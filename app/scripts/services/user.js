'use strict';

/**
 * @ngdoc function
 * @name testProjectApp.service:UserService
 * @description
 * # UserService
 * Service of the testProjectApp
 */
angular.module('testProjectApp')
  .service('UserService', function (store, $q) {
    
    var userService = this;
    userService.userInfo = store.get('profile');
    
    var defer = $q.defer();

    userService.setUser = function(data) {
      userService.userInfo = data;
      if (defer) {
        defer.resolve(data);
      }
    };
    
    userService.getUser = function() {
      if (userService.userInfo) {
        defer.resolve(userService.userInfo);
      }
      return defer.promise;
    };

    userService.clearUser = function() {
      defer = $q.defer();
      userService.userInfo = null;
    };
    
  });
