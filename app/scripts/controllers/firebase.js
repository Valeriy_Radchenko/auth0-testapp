'use strict';

/**
 * @ngdoc function
 * @name testProjectApp.controller:FirebaseCtrl
 * @description
 * # FirebaseCtrl
 * Controller of the testProjectApp
 */

angular.module('testProjectApp')
  .controller('FirebaseCtrl', function ($scope, Config, store, NetworkService, $q, $timeout) {
    
    function getDelegationToken() {
      var defer = $q.defer();
      
      if (store.get('delegation_token')) {
        defer.resolve(store.get('delegation_token'));
      } else {
        var data = {
        'scope': 'openid name email nickname',
        'grant_type': 'urn:ietf:params:oauth:grant-type:jwt-bearer',
        'client_id': Config.clientId,
        'api_type': 'firebase',
        'id_token': store.get('token')
        };

        NetworkService.post('/delegation', data, function(result, response) {

          if (result) {
            store.set('delegation_token', response.id_token);
            defer.resolve(response.id_token);
            
          }

        });
      }
      return defer.promise;
    }
    
    
    getDelegationToken().then(function(token) {
      
      usersRef.authWithCustomToken(token, function(common, tokenData) {
        $scope.account = tokenData.auth.token.aud;
        //$timeout(function() {});
        usersRef.orderByValue().on("value", function(snapshot) {
          $scope.baseData = [];
          snapshot.forEach(function(data) {
            var item = {
              key: data.key(),
              value: data.val()
            };
            
            
            $scope.baseData.push( item );
            
          });
          $timeout(function() {});
        });

      }, function(e) {
        console.log(e);
      });
      
    });
    
    
    
  });