'use strict';

/**
 * @ngdoc function
 * @name testProjectApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the testProjectApp
 */
angular.module('testProjectApp')
  .controller('UsersCtrl', function ($scope, Config, NetworkService, $location, store, UserService) {
    
    $scope.getUsers = function() {
      var params = {
        'page': 0,
        'include_totals': true,
        'search_engine': 'v2'
      };
      NetworkService.get('/api/v2/users', params, function(result, response) {
        if (result) {
          $scope.users = response.users;
        }
      }, {'Authorization': 'Bearer ' + Config.managementToken});

    };
    $scope.getUsers();
    
    $scope.showUser = function(user) {
      UserService.clearUser();
      UserService.setUser(user);
      $location.path('/user' + user.user_id);
      
    };
    
  });
