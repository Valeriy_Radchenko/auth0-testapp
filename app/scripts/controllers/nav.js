'use strict';

/**
 * @ngdoc function
 * @name testProjectApp.controller:NavCtrl
 * @description
 * # NavCtrl
 * Controller of the testProjectApp
 */
angular.module('testProjectApp')
  .controller('NavCtrl', function ($scope, auth) {

    $scope.auth = auth;
    
  });
