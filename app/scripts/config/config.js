'use strict';

/**
 * @ngdoc function
 * @name testProjectApp.config:Config
 * @description
 * # Config
 * Config of the testProjectApp
 */
angular.module('testProjectApp')
  .service('Config', function() {
    var Config = this;
    
    Config.servicePath = 'https://vradchenko.eu.auth0.com';
    Config.apiPath = '/api/v2';
    
    Config.clientId = 'ZdtnBi0gBlMc1BIJWUZRxr34w5aL6S09';
    Config.connection = 'Username-Password-Authentication';
    Config.grantType = 'password';
    Config.scope = 'openid';
    
    
    Config.managementToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiI3YlF0bm9MMGZRUFZEVmpwZzRhdHlhM3ZXV2ZHN2F2VCIsInNjb3BlcyI6eyJ1c2VycyI6eyJhY3Rpb25zIjpbInJlYWQiLCJ1cGRhdGUiLCJkZWxldGUiLCJjcmVhdGUiXX0sImNvbm5lY3Rpb25zIjp7ImFjdGlvbnMiOlsiY3JlYXRlIiwicmVhZCIsInVwZGF0ZSIsImRlbGV0ZSJdfX0sImlhdCI6MTQ2NjUxODk1MSwianRpIjoiZTVjNjllYmRiNzhhZTQ5MTA2NzAxN2U5NjI1MGIwYWQifQ.GYsvVtik5CFM4pjLlrgC8RLFnsnkysS7ws_AiD6o_0k';
    
    
  });
