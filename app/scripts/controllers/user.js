'use strict';

/**
 * @ngdoc function
 * @name testProjectApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the testProjectApp
 */
angular.module('testProjectApp')
  .controller('UserCtrl', function ($scope, UserService, NetworkService, Config, $location, $filter) {

    UserService.getUser().then(function(user) {
      $scope.user = user;
      $scope.userFields = {
        'name': user.name || '(empty)',
        'nickname': user.nickname || '(empty)',
        'email': user.email || '(empty)',
        'updated': $filter('date')(user.updated_at, 'h:mm a MM/dd/yyyy')
      };
    });
    
    $scope.deleteUser = function() {
      NetworkService.delete(Config.apiPath + '/users/' + $scope.user.user_id, false, function(result, response) {
        if (result) {
          $location.path('/users');
        }
      }, {'Authorization': 'Bearer ' + Config.managementToken});
    };
    
    $scope.editUser = function() {
      $location.path('/edit-user:'+$scope.user.user_id);
    };
    
  });
