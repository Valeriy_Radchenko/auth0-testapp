'use strict';

/**
 * @ngdoc function
 * @name testProjectApp.service:NetworkService
 * @description
 * # NetworkService
 * Service of the testProjectApp
 */
angular.module('testProjectApp')
  .service('NetworkService',['$http', '$httpParamSerializer', 'Config',
    function($http, $httpParamSerializer, Config) {
    var $network = this;

    $network.servicePath = Config.servicePath;

    var CANCEL_TIMEOUT = Config.CANCEL_TIMEOUT;

    $network.get = function(methodName, params, callback, headers) {
      callback = callback || function(){};

      var path = $network.servicePath + methodName;

      if (params) {
        path = $network.servicePath + methodName + '?' + $httpParamSerializer(params);
      }
      
      var httpParams = { 'method': 'get'
                       , 'url': path
                       , timeout: CANCEL_TIMEOUT
                       };
      
      if (headers) {
        httpParams.headers = headers;
      }
      
      $http(httpParams)

              .success(function(result, status, headers) {
                callback(true, result);
              })

              .error(function(data, status, headers, config, statusText) {
                callback(false,data,status);
              });

    };

    $network.post = function(methodName, params, callback, headers) {

      callback = callback || function(){};

      params = params || {};

      var path = $network.servicePath + methodName;
      
      var httpParams = { 'method': 'post'
                       , 'url': path
                       , 'data':  $httpParamSerializer(params)
                       , 'headers' : {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
                       , timeout: CANCEL_TIMEOUT
                       };
              
      if (headers) {
        httpParams.headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
        httpParams.headers = headers;
      }

      $http(httpParams)
              .success(function(result, status, headers) {
                callback(true, result);
              })
              .error(function(data, status, headers, config, statusText) {
                callback(false,data,status);
              });

    };

    $network.put = function(methodName, params, callback, headers) {

      callback = callback || function(){};

      params = params || {};

      var path = $network.servicePath + methodName;
      
      var httpParams = { 'method': 'put'
                       , 'url': path
                       , 'data':  params
                       , timeout: CANCEL_TIMEOUT
                       };
           
      if (headers) {
        httpParams.headers = headers;
      }

      $http(httpParams)
              .success(function(result, status, headers) {
                callback(true, result);
              })
              .error(function(data, status, headers, config, statusText) {
                callback(false,data,status);
              });

    };
    
    $network.delete = function(methodName, params, callback, headers) {

      callback = callback || function(){};

      var path = $network.servicePath + methodName;
      
      var httpParams = { 'method': 'delete'
                       , 'url': path
                       , timeout: CANCEL_TIMEOUT
                       };
           
      if (headers) {
        httpParams.headers = headers;
      }
      
      if (params) {
        httpParams.data = params;
      }

      $http(httpParams)
              .success(function(result, status, headers) {
                callback(true, result);
              })
              .error(function(data, status, headers, config, statusText) {
                callback(false,data,status);
              });

    };
    
    $network.patch = function(methodName, params, callback, headers) {

      callback = callback || function(){};

      params = params || {};

      var path = $network.servicePath + methodName;
      
      var httpParams = { 'method': 'patch'
                       , 'url': path
                       , 'data':  params
                       , timeout: CANCEL_TIMEOUT
                       };
           
      if (headers) {
        httpParams.headers = headers;
      }

      $http(httpParams)
              .success(function(result, status, headers) {
                callback(true, result);
              })
              .error(function(data, status, headers, config, statusText) {
                callback(false,data,status);
              });

    };

  }]);
