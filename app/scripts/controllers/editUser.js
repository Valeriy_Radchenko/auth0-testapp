'use strict';

/**
 * @ngdoc function
 * @name testProjectApp.controller:EditUserCtrl
 * @description
 * # EditUserCtrl
 * Controller of the testProjectApp
 */
angular.module('testProjectApp')
  .controller('EditUserCtrl', function ($scope, $location, UserService, $filter, NetworkService, Config) {

    UserService.getUser().then(function(user) {
      $scope.user = user;
      $scope.form = {
        'name': user.name || '(empty)',
        'nickname': user.nickname || '(empty)',
        'email': user.email || '(empty)',
        'updated': $filter('date')(user.updated_at, 'h:mm a MM/dd/yyyy')
      };
    });
    
    $scope.update = function(form) {

      var data = {
        "connection": "Username-Password-Authentication",
        "email": form.email,
        "client_id": Config.clientId
      };

      NetworkService.patch(Config.apiPath + '/users/' + $scope.user.user_id, data, function(result, response) {
        if (result) {
          $scope.user.email = form.email;
          $location.path('/user'+$scope.user.user_id);
        } else {
          $scope.error = response;
        }
      }, {'Authorization': 'Bearer ' + Config.managementToken});

    };
    
    $scope.cancel = function() {
      $location.path('/user'+$scope.user.user_id);
    };
    
    
  });
